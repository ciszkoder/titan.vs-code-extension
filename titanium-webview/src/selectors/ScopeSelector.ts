/******************************************************************************
 * Copyright (c) 2000-2024 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
/**
* @author Csilla Farkas
*/

import { Selector } from "./Selector";
import { vscode } from "../common/constants";

export class ScopeSelector implements Selector {
    readonly ttcn3Modules: Record<string, string>;
    readonly asn1Modules: Record<string, string>;
    private select: HTMLSelectElement;

    constructor(ttcn3Modules: Record<string, string> , asn1Modules: Record<string, string> ) {
        this.asn1Modules = asn1Modules;
        this.ttcn3Modules = ttcn3Modules;
        this.select = this.createSelect();
    }

    private createSelect() {
        const select = document.createElement('select');
        select.id = 'modules';
        select.classList.add('selector');
        const placeholder = new Option('Measurement scope', '', true, true);
        placeholder.disabled = true;
        const fullProjectOption = new Option('Full project statistics', 'project');
        select.add(placeholder);
        select.add(fullProjectOption);
        select.add(this.fillOptionGroup('TTCN3 Modules','ttcnModules', this.ttcn3Modules));
        select.add(this.fillOptionGroup('ASN1 Modules','asnModules', this.asn1Modules));

        return select;
    }

    private fillOptionGroup(label: string, optgroupId: string, moduleList: Record<string, string>): HTMLOptGroupElement {
        const group = document.createElement('optgroup');
        group.id = optgroupId;
        group.label = label;
        
        const state = vscode.getState();
        for (const [ name, uri ] of Object.entries(moduleList)) {
            const option = new Option(name, uri);
            if (state?.identifier === uri) {
                option.selected = true;
            }
            group.appendChild(option);
        }

        return group;
    }

    public getSelectedValue(): string {
        return this.select.value;
    }

    public getSelect(): HTMLSelectElement {
        return this.select;
    }
}
