/******************************************************************************
 * Copyright (c) 2000-2024 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
/**
* @author Csilla Farkas
*/

import { MetricsFullStatTable } from "./MetricsFullStatTable";
import { FullProjectStatistics } from "../common/interfaces";

export class ProjectStatTable extends MetricsFullStatTable {
    constructor(data: FullProjectStatistics, projectName: string) {
      super(data, projectName);
      this.title = `Project: ${projectName}`;
    }
  
    public showTable(container: Element): void {
      this.removePrevTable();
      this.refreshTableTitle(container);
      
      container.appendChild(this.table);
    }
}