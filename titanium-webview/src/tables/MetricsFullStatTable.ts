/******************************************************************************
 * Copyright (c) 2000-2024 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
/**
* @author Csilla Farkas
*/

import { FullStatistics, MetricDataStat, MetricDataVal, StatisticColumns } from "../common/interfaces";
import { MetricsDataTable } from "./MetricsDataTalbe";


export abstract class MetricsFullStatTable implements MetricsDataTable {
    readonly projectName: string;
    readonly data: FullStatistics;
    readonly table: HTMLTableElement;
    title: string = '';
    readonly headers: Map<string, string> = new Map([
      ['METRICS', 'Metrics'],
      ['TOTAL', 'Total'],
      ['MEAN', 'Mean'],
      ['MAX', 'Max'],
      ['DEV', 'Std.Dev']
    ]);
  
    readonly metrics: Map<string, string> = new Map([
      ['projectMetrics', 'Project Metrics'],
      ['moduleMetrics', 'Module Metrics'],
      ['functionMetrics', 'Function Metrics'],
      ['testcaseMetrics', 'Testcase Metrics'],
      ['altstepMetrics', 'Altstep Metrics']
    ]);
  
    constructor(data: FullStatistics, projectName: string) {
      this.projectName = projectName;
      this.data = data;
      this.table = this.createTable();
    }
  
    protected createTableHeader(table: HTMLTableElement): void {
      const tableHeader = document.createElement('thead');
      const headerRow = tableHeader.insertRow();
      for (const [id, textContent] of this.headers) {
        const cell = document.createElement('th');
        cell.id = id;
        cell.textContent = textContent;
        cell.scope = 'col';
        headerRow.appendChild(cell);
      }
      table.appendChild(tableHeader);
    }
  
    protected createStatRows(data: (MetricDataStat | MetricDataVal)[]): HTMLTableRowElement[] {
      const rows: HTMLTableRowElement[] = [];
      data.forEach(stats => {
        const row = document.createElement('tr');
        for (const [id, ] of this.headers) {
          const cell = row.insertCell();
          if (id === 'METRICS') {
            cell.textContent = stats.name;
            cell.scope = 'row';
          } else {
            const key = id as keyof StatisticColumns;
            const cellContent = 'columns' in stats.statistics ? stats.statistics.columns[key] : stats.statistics[key];
            cell.textContent = cellContent ? cellContent.toFixed(2).toString() : '-';
          }
        }
        rows.push(row);
      });

      return rows;
    }

    protected createTableBody(table: HTMLTableElement): void {
      const tableBody = document.createElement('tbody');
      for (const [metricName, statList] of Object.entries(this.data)) {
        if (this.metrics.has(metricName)) {
          const titleRow = tableBody.insertRow();
          const titleCell = titleRow.insertCell();
          titleCell.textContent = this.metrics.get(metricName)!;
          titleCell.classList.add('title-cell');
          const nofCols = this.headers.size;
          titleCell.setAttribute('colspan', nofCols?.toString());

          const statRows = this.createStatRows(statList);
          statRows.forEach(row => tableBody.appendChild(row));
        }
      }

      table.appendChild(tableBody);
    }

    protected createTable(): HTMLTableElement {
      const table = document.createElement('table');
      table.id = 'stat-table';
      this.createTableHeader(table);
      this.createTableBody(table);
      return table;
    }

    protected removePrevTable(): void {
      const oldTable = document.querySelector('#stat-table');
      oldTable?.remove();
    }

    protected refreshTableTitle(container: Element): void {
      const prevTitle = document.querySelector('#stat-table-title');
      prevTitle?.remove();

      const tableTitle = document.createElement('h3');
      tableTitle.id = 'stat-table-title';
      tableTitle.textContent = this.title;
      container.appendChild(tableTitle);
    }

    public showTable(container: Element) {};
}





