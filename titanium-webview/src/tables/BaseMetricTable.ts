/******************************************************************************
 * Copyright (c) 2000-2024 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
/**
* @author Csilla Farkas
*/

import { Metrics } from '../common/interfaces';

export class BaseMetricTable {
    private table: HTMLTableElement;
    private headers: Map<string, string> = new Map([
      ['METRICS', 'Metrics'],
      ['VALUE', 'Value']
    ]);
  
    constructor(private data: Metrics) {
      this.table = this.createTable();
    }
  
    private createTableHeader() {
      const header = document.createElement('thead');
      const row = header.insertRow();
      for (const [id, text] of this.headers) {
        const cell = document.createElement('th');
        cell.textContent = text;
        row.appendChild(cell);
      }
  
      return header;
    }
  
    private createTableBody() {
      const tableBody = document.createElement('tbody');
      const titleRow = tableBody.insertRow();
      const titleCell = titleRow.insertCell();
      titleCell.textContent = this.data.name;
      titleCell.classList.add('title-cell');
      const nofCols = this.headers.size;
      titleCell.setAttribute('colspan', nofCols?.toString());
  
      for (const [metric, value] of Object.entries(this.data.metrics)) {
        const row = tableBody.insertRow();
        row.insertCell().textContent = metric;
        row.insertCell().textContent = value.toString();
      }
  
      return tableBody;
    }
  
    private createTable() {
      const table = document.createElement('table');
      table.id = `${this.data.type}-metric-table`;
      const header = this.createTableHeader();
      const body = this.createTableBody();
      table.appendChild(header);
      table.appendChild(body);
  
      return table;
    }
    
    public showTable(container: Element) {
      const prevTable = document.querySelector(`#${this.table.id}`);
      prevTable?.remove();
      container.appendChild(this.table);
    }
  }