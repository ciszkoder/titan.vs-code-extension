/******************************************************************************
 * Copyright (c) 2000-2024 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
/**
* @author Csilla Farkas
*/

import { FullStatistics } from "../common/interfaces";

export interface MetricsDataTable {
    readonly projectName: string;
    readonly data: FullStatistics;
    readonly table: HTMLTableElement;
    readonly title: string;
    readonly headers: Map<string, string>;
    readonly metrics: Map<string, string>;

    showTable(container: Element): void;
}