/******************************************************************************
 * Copyright (c) 2000-2024 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
/**
* @author Csilla Farkas
*/

import { UIManager } from "./UIManager";
import { vscode } from "./common/constants";

window.addEventListener('message', event => {
  const { scope, identifier, projectName, payload } = event.data;

  vscode.setState({scope, identifier});
  const ui = new UIManager(scope, projectName, payload);
  ui.render();
});

window.addEventListener('load', main);

function main() {
  const prevState = vscode.getState();
  if (prevState && prevState.scope) {
    vscode.postMessage({
      scope: prevState.scope,
      identifier: prevState.identifier,
      targetName: null,
      targetType: null
    });
  } else {
    vscode.postMessage({
      scope: 'project',
      identifier: 'project',
      targetName: null,
      targetType: null
    });
  }
}
