/******************************************************************************
 * Copyright (c) 2000-2024 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
/**
* @author Csilla Farkas
*/

import { MeasureButton } from "./MeasureButton";
import { ScopeSelector } from "../selectors/ScopeSelector";
import { vscode } from "../common/constants";

export class FullStatMeasureButton extends MeasureButton {
    constructor(selector: ScopeSelector) {
        super(selector);
        this.getButton().addEventListener('click', this.handleOnClick);
    }
    protected handleOnClick = (event: Event) => {
        const selectedValue = this.selector.getSelectedValue();
        const isProject = selectedValue === 'project';
        const message = {
            scope: isProject ? selectedValue : 'module',
            identifier: isProject ? '' : selectedValue,
            targetName: null,
            targetType: null
        };

        vscode.postMessage(message);
    };
}