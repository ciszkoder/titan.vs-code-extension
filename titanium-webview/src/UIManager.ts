import { FullStatistics, Metrics, FullProjectStatistics, FullModuleStatistics } from "./common/interfaces";
import { ScopeSelector } from "./selectors/ScopeSelector";
import { FullStatMeasureButton } from "./buttons/FullStatMeasureButton";
import { ProjectStatTable } from "./tables/ProjectStatTable";
import { ModuleStatTable } from "./tables/ModuleStatTable";
import { BaseMetricTable } from "./tables/BaseMetricTable";

/******************************************************************************
 * Copyright (c) 2000-2024 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
/**
* @author Csilla Farkas
*/

export class UIManager {
    private moduleSection = document.querySelector('#modules-container')!;
    private metricsSection = document.querySelector('#metrics-container')!;
  
    constructor(
      private scope: string,
      private projectName: string,
      private data: FullStatistics | Metrics
    ) {}
  
    private renderScopeSelector() {
      const prevScopeSelector = document.querySelector('#scopeselector-container');
      prevScopeSelector?.remove();
  
      const selectorContainer = document.createElement('div');
      selectorContainer.id = 'scopeselector-container';
      selectorContainer.classList.add('selector-container');
      const scopeSelector = new ScopeSelector((this.data as FullStatistics).ttcn3Modules, (this.data as FullStatistics).asn1Modules);
      selectorContainer.appendChild(scopeSelector.getSelect());
      selectorContainer.appendChild(new FullStatMeasureButton(scopeSelector).getButton());
      this.moduleSection.appendChild(selectorContainer);
    }
  
    private renderProjectStatPage() {
      this.removeMetricTables();
      this.renderScopeSelector();
      const table = new ProjectStatTable(this.data as FullProjectStatistics, this.projectName);
      table.showTable(this.metricsSection);
    }
    
    private renderModuleStatPage() {
      this.removeMetricTables();
      this.renderScopeSelector();
      const table = new ModuleStatTable(this.data as FullModuleStatistics, this.projectName);
      table.showTable(this.metricsSection);
    }
    
    private renderMetrics() {
      const selectorContainer = document.querySelector(`#${(this.data as Metrics).type}-container`);
      if (selectorContainer) {
        const table = new BaseMetricTable((this.data as Metrics));
        table.showTable(selectorContainer);
      }
    }
    
    private removeMetricTables() {
      const functionContainer = document.querySelector('#function-container');
      functionContainer?.remove();
      
      const altstepContainer = document.querySelector('#altstep-container');
      altstepContainer?.remove();
      
      const testcaseContainer = document.querySelector('#testcase-container');
      testcaseContainer?.remove();
    }
  
    public render() {
      if (this.scope === 'module') {
        if ('metrics' in this.data) {
          this.renderMetrics();
          return;
        }
    
        this.renderModuleStatPage();
        return;
      }
    
      this.renderProjectStatPage();
    }
}