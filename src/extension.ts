/******************************************************************************
 * Copyright (c) 2000-2024 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/

/**
 * @author Miklos Magyari
 * @author Adam Knapp
 * @author Csilla Farkas
 */

import { ExtensionContext, Uri, commands, languages, tasks, window, workspace } from 'vscode';
import client from './client/titanClient';
import { WorkspaceEventManager, registerWorkspaceEvents } from './events/WorkspaceEventManager';
import { registerCommands } from './commands/titanCommands';
import { CommandManager } from './commands/CommandManager';
import { DocCommentViewProvider, docCommentViewProvider } from './document-comment-view/DocCommentViewProvider';
import { WindowEventManager, registerWindowEvents } from './events/WindowEventManager';
import { ExcludedFileDecorationProvider } from './file-exclusion/ExcludedFileDecorationProvider';
import { TpdFileDecorationProvider } from './file-exclusion/exclude-by-tpd/TpdFileDecorationProvider';
import { mainExclusionManager } from './file-exclusion/MainExclusionManager';
import { titanTypeHierarchyProvider } from './type-hierarchy/TitanTypeHierarchy';
import { ClientManager } from './client/ClientManager';
import { MetricsProvider } from './titanium-metrics-view/MetricsProvider';
import { sendEnvironmentVariables } from './client/Environment';
import { BuildTaskProvider } from './tasks/BuildTaskProvider';
import { Disposable } from 'vscode-languageclient';

export var ctx: ExtensionContext;
let taskProvider: Disposable|undefined;

export function activate(context: ExtensionContext) {
	const workspaceEventManager = new WorkspaceEventManager();
	context.subscriptions.push(workspaceEventManager);

	const commandManager = new CommandManager();
	context.subscriptions.push(commandManager);

	const excludedFileDecorationPorvider = new ExcludedFileDecorationProvider();
	context.subscriptions.push(excludedFileDecorationPorvider);

	const tpdFileDecorationProvider = new TpdFileDecorationProvider();
	context.subscriptions.push(tpdFileDecorationProvider);

	context.subscriptions.push(languages.registerTypeHierarchyProvider('ttcn', titanTypeHierarchyProvider));

	context.subscriptions.push(window.registerWebviewViewProvider(DocCommentViewProvider.viewType, docCommentViewProvider));

	const metricsProvider = new MetricsProvider(context.extensionUri);
	context.subscriptions.push(window.registerWebviewViewProvider(MetricsProvider.viewType, metricsProvider));

	const windowEventManager = new WindowEventManager();
	context.subscriptions.push(windowEventManager);

	taskProvider = tasks.registerTaskProvider(BuildTaskProvider.buildTaskProviderType, new BuildTaskProvider());

	registerWorkspaceEvents(workspaceEventManager);
	registerCommands(commandManager, context);
	registerWindowEvents(windowEventManager);

	client.start().then(() => {
		if (client.isRunning()) {
			sendEnvironmentVariables();
			ClientManager.ready();
			mainExclusionManager.init();

			console.log('Titan is now active!');
		}
	});

	ctx = context;
}

export function deactivate(): Thenable<void> | undefined {
	if (taskProvider) {
		taskProvider.dispose();
	}
	if (!client) {
		return undefined;
	}
	client.stop();
}
