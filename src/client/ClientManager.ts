/******************************************************************************
 * Copyright (c) 2000-2024 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
/**
* @author Csilla Farkas
* @author Miklos Magyari
*/

import { LanguageClient, TextDocumentIdentifier } from "vscode-languageclient/node";
import { Position, Uri, TypeHierarchyItem } from "vscode";
import client from "./titanClient";
import { ExcludedState } from "../common/types";
import { Modal } from "../common/Modal";

export class ClientManager {
    private static _isReady: boolean = false;
    private _methods = {
        documentComment: 'titan/documentComment',
        exclude: 'titan/setExcludedState',
        activateTpd: 'titan/activateTpd',
        deactivateTpd: 'titan/deactivateTpd',
        generateDocComment: 'titan/generateDocumentComment',
        prepareTypeHierarchy:  'textDocument/prepareTypeHierarchy',
        supertypesTypeHierarchy: 'typeHierarchy/supertypes',
        subtypesTypeHierarchy: 'typeHierarchy/subtypes',
        inlayHint: 'textDocument/inlayHint',
        metricData: 'titan/getMetricData',
        variables: 'titan/environmentVariables',
        folderConfig: 'titan/folderConfig'
    };

    constructor(private _client: LanguageClient) {};

    public static ready(): void {
        this._isReady = true;
    }

    public static isReady(): boolean {
        return this._isReady;
    }

    public async getDocumentComment(param: any): Promise<string | void> {
        const response = await this._client.sendRequest(this._methods.documentComment, param) as string;
        return response;
    };

    public notifyServerIfExcludeStateChanged(excludedStates: ExcludedState[]) {
        this._client.sendNotification(this._methods.exclude, { excludedStates });
    }

    public async parseTpd(tpd: Uri | null) {
        const tpdPath = tpd ? tpd.fsPath : tpd;
        // Sending null as tpdPath triggers full project analysis
        const {success, result} = await this._client.sendRequest(this._methods.activateTpd, { tpdPath }) as {success: boolean, result: string[] | string};
        if (success && result.length) {
            return result as string[];
        }
        if (tpd) {
            await Modal.failedToParseTpd(tpd, result as string);
        }
    }

    public async deactivateTpd() {
        await this._client.sendRequest(this._methods.deactivateTpd);
    }

    public async generateDocComment(requestParams: {identifier: TextDocumentIdentifier, indentation: string, position: Position}) {
        const response = await this._client.sendRequest(this._methods.generateDocComment, requestParams) as string;
        return response;
    }

    public async prepareTypeHierarchy(requestParams: {textDocument: TextDocumentIdentifier, position: Position}) {
        const typeHierarchyItems = await this._client.sendRequest(this._methods.prepareTypeHierarchy, requestParams);
        return typeHierarchyItems;
    }

    public async resolveSupertypes(item: TypeHierarchyItem) {
        const convertedItem = {
            ...item,
            uri: item.uri.toString()
        };
        const supertypesResult = await this._client.sendRequest(this._methods.supertypesTypeHierarchy, {item: convertedItem});
        return supertypesResult;
    }

    public async resolveSubtypes(item: TypeHierarchyItem) {
        const convertedItem = {
            ...item,
            uri: item.uri.toString()
        };
        const subtypeResult = await this._client.sendRequest(this._methods.subtypesTypeHierarchy, {item: convertedItem});
        return subtypeResult;
    }

    public async getInlayHints(requestParams: {textDocument: TextDocumentIdentifier, range: {start: Position, end: Position}}) {
        const hints = await this._client.sendRequest(this._methods.inlayHint, requestParams);
        return hints;
    }

    public async getMetricData(requestParams: {scope: string, targetName: string, targetType: string, identifier: TextDocumentIdentifier}) {
        const data = await this._client.sendRequest(this._methods.metricData, requestParams);
        return data;
    }

    public sendEnvironmentVariables() {
        const variables = Object.entries(process.env).map(([name, value]) => ({
            name, value
        }));
        client.sendNotification(this._methods.variables, {
            variables
        });
    }

    public sendFolderConfig(uri: Uri, text: string) {
        let folderConfig = {
            uri: '',
            config: {
                definedSymbols: [],
                undefinedSymbols: [],
                compilerFlags: {}
            }
        };
        const jsonText = `{ "uri": "${uri}", "config": ${text}}`;
        let json;
        try {
            json = JSON.parse(jsonText);
        } catch(e) {
            console.log(e);
            return;
        }
        const definedSymbols = json.config.definedSymbols;
        folderConfig.uri = uri.toString();
        if (definedSymbols !== undefined) {
            folderConfig.config.definedSymbols = definedSymbols;
        }
        const undefinedSymbols = json.config.undefinedSymbols;
        if (undefinedSymbols !== undefined) {
            folderConfig.config.undefinedSymbols = undefinedSymbols;
        }
        const compilerFlags = json.config.compilerFlags;
        if (compilerFlags !== undefined) {
            folderConfig.config.compilerFlags = compilerFlags;
        }

        client.sendNotification(this._methods.folderConfig, {
            uri: folderConfig.uri,
            config: folderConfig.config
        });
    }
}

export const clientManager = new ClientManager(client);