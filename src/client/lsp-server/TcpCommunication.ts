/******************************************************************************
 * Copyright (c) 2000-2024 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
/**
 * @author Csilla Farkas
 */

import { StreamInfo } from 'vscode-languageclient/node';
import * as net from 'net';
import {settings} from "../../common/Settings";
import { LspServerLauncher } from "./LspServerLauncher";

export class TcpCommunication extends LspServerLauncher {
    private port: number | null = null;
    private socket: string =  '--socket 127.0.0.1:';
    private debug: string = '-agentlib:jdwp=transport=dt_socket,address=127.0.0.1:' + settings.debugPort + ',server=y,suspend=n';

    async launchServer(): Promise<StreamInfo> {
        return new Promise((resolve, reject) => {
            const socketListener = net.createServer((socket) => {
                resolve({
                    reader: socket,
                    writer: socket
                });
                socket.on('error', (err) => console.error('net.Socket: ', err));
            });
            
            socketListener.on('listening', () => {
                const { port, address } = socketListener.address() as net.AddressInfo;
                this.port = port;
                console.log(`Titan extension language client socket: ${address === '::' ? 'localhost' : address}, port: ${port}`);
            });
    
            socketListener.on('error', (err) => {
                console.error('net.Server: ', err);
            });
    
            socketListener.listen(settings.serverPort, () => {
                const args = [
                    this.minHeapSize,
                    this.maxHeapSize,
                    // debug,
                    this.jar,
                    LspServerLauncher._serverExecutableFilePath,
                    this.socket + this.port
                ];
                const childProcess = this.createChildProcess(args, this.childProcessOptions);
                this.setupLogging(childProcess);
                process.on('exit', () => childProcess.kill());
            });
        });
    }
}