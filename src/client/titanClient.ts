/******************************************************************************
 * Copyright (c) 2000-2024 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
/**
 * @author Miklos Magyari
 * @author Csilla Farkas
 */

import { workspace } from 'vscode';
import { LanguageClient, LanguageClientOptions } from 'vscode-languageclient/node';
import { TitanLspServer } from './lsp-server/TitanLspServer';
import { provideDefinitionSignature } from '../middleware/DefinitionMiddleware';
import { provideRenameEditsSignature } from '../middleware/RenameMiddleware';

const serverId = 'ttcnserver';
const serverName = 'ttcn3server';

const clientOptions: LanguageClientOptions = {
    documentSelector: [
        { scheme: 'file', language: 'ttcn'},
        { scheme: 'file', language: 'asn1'},
        { scheme: 'file', language: 'cfg' }
    ],
    
    synchronize: {
        configurationSection: 'titan',
        fileEvents: workspace.createFileSystemWatcher('**/*'),
    },

    middleware: {
        provideDefinition: async (document, position, token) => {
            return provideDefinitionSignature(document,position,token);
            
        },
        provideRenameEdits: async(document, position, newName, token) => {
            return provideRenameEditsSignature(document, position, newName, token);
        }
    }
};

const client = new LanguageClient (
    serverId, 
    serverName, 
    TitanLspServer.start,
    clientOptions
);

export default client;