/******************************************************************************
 * Copyright (c) 2000-2024 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
/**
* @author Csilla Farkas
*/

export class Timer {
    private _timeout: NodeJS.Timeout | null | number = null;

    constructor(private _delay: number = 1000) {};

    public startTimer = (cbForTimer: (...args: any) => any, ...args: any) => {
        this._timeout = setTimeout(cbForTimer, this._delay, ...args);
    };

    public clearTimer(): void {
        if (this._timeout) {
            clearTimeout(this._timeout);
        }
    }
}
