/******************************************************************************
 * Copyright (c) 2000-2024 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
/**
 * @author Csilla Farkas
 */

import * as path from 'path';
import { workspace, Uri, extensions } from 'vscode';
import { ConfigKey, ConnectionKind } from './types';
import { Modal } from './Modal';

class Settings {
    private _configSectionName = ConfigKey.titanExtension;
    private _extensionPath;
    private _configuration;
    private _serverPort: number = 55555;
    private _debugPort: number = 54321;
    private _defaultLogDir;
    private _defaultLogFileName = 'titan-languageserver-java.log';
    private _supportedFileTypes = new Set<string>(['asn', 'asn1', 'ttcn', 'ttcn3', 'ttcnpp', 'ttcnin']);

    constructor() {
        this._configuration = workspace.getConfiguration(this._configSectionName);
        this._extensionPath = extensions.getExtension('eclipse-titan.titan')?.extensionPath;
        this._defaultLogDir = path.join(this._extensionPath!, 'logs');
    }

    get serverPort(): number {
        const serverPortFromConfig = this._configuration.get('connection.serverPort') as number;
        const serverPort = serverPortFromConfig ? serverPortFromConfig : this._serverPort;
        return serverPort;
    }

    get debugPort(): number {
        const debugPortFromConfig = this._configuration.get('debugPort') as number;
        const debugPort = debugPortFromConfig ? debugPortFromConfig : this._debugPort;
        return debugPort;
    }

    get logDir(): string {
        const logDirFromConfig = this._configuration.get('connection.logDir') as string;
        if (logDirFromConfig) {
            const dir = path.normalize(logDirFromConfig);
            if (!path.isAbsolute(dir)) {
                Modal.invalidLogFilePath(path.join(logDirFromConfig, this._defaultLogFileName));
                return this._defaultLogDir;
            }
            return dir;
        }
        return this._defaultLogDir;
    }

    get logFileName(): string {
        return this._defaultLogFileName;
    }

    get extensionPath(): string {
        return this._extensionPath!;
    }

    get projectRootUri(): Uri | null {
        const workspaceFolders = workspace.workspaceFolders;
        const rootUri = workspaceFolders && workspaceFolders?.length > 0 ? workspaceFolders[0].uri : null;
        return rootUri;
    }

    get connectionKind(): ConnectionKind {
        const connectionKind = this._configuration.get('connection.connectionKind') as ConnectionKind;
        return connectionKind;
    }

    get titaniumEnabled(): boolean {
        const titaniumEnabled = workspace.getConfiguration('titan').get('enableTitanium') as boolean;
        return titaniumEnabled;
    }

    isSupportedFileType(fileType: string): boolean {
        return this._supportedFileTypes.has(fileType);
    }
}

export const settings = new Settings();
