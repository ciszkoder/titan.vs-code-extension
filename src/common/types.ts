/******************************************************************************
 * Copyright (c) 2000-2024 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
/**
 * 
 * @author Csilla Farkas
 */


export type ExcludedState = {
    identifier: {
        uri: string
    },
    isExcluded: boolean
};

export enum TpdContextValue {
    activated = 'activated',
    deactivated = 'deactivated'
}

export enum ConfigKey {
    titanExtension = 'titan.extension',
    excludeFromBuild = 'titan.extension.excludedFromBuild',
    tpdFiles = 'titan.compiler.tpdFiles',
    activatedTpd = 'titan.compiler.activatedTpd',
    excludeByTpd = 'titan.compiler.excludeByTpd',
    editor = 'editor',
    compiler = 'titan.compiler'
}

export enum ContextKey {
    excludeByTpd = 'excludeByTpd',
    excludedItems = 'excludedItems',
    tempExcludedItems = 'tempExcludedItems',
    tempIncludedItems = 'tempIncludedItems'
}

export enum ViewNames {
    tpd = 'tpdview',
    metrics = 'metricsview'
}

export enum ConnectionKind {
    tcp = 'tcp',
    stdio = 'stdio'
}
