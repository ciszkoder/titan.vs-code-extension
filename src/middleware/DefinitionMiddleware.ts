/******************************************************************************
 * Copyright (c) 2000-2024 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
/**
 * @author Miklos Magyari
 */

import { window } from 'vscode';
import { DefinitionRequest, ProvideDefinitionSignature } from "vscode-languageclient";
import client from "../client/titanClient";

export const provideDefinitionSignature: ProvideDefinitionSignature = (document, position, token) => {
     return client.sendRequest(DefinitionRequest.type, client.code2ProtocolConverter.asTextDocumentPositionParams(document, position), token)
          .then((result) => {
               if (token.isCancellationRequested) {
                    return null;
               }
               return client.protocol2CodeConverter.asDefinitionResult(result);
          }, (error) => {
               window.showErrorMessage(error.message);
               return client.handleFailedRequest(DefinitionRequest.type, token, error.message, null, false);
          });
};