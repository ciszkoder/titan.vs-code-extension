/******************************************************************************
 * Copyright (c) 2000-2024 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
/**
 * @author Miklos Magyari
 */

import { ProvideRenameEditsSignature, RenameParams, RenameRequest } from 'vscode-languageclient';
import client from "../client/titanClient";

export const provideRenameEditsSignature: ProvideRenameEditsSignature  = (document, position, newName, token) => {
     let params: RenameParams = {
          textDocument: {
               uri: document.uri.toString()
          },
          position: position,
          newName
     };
     return client.sendRequest(RenameRequest.type, params, token)
          .then(
               (result) => {
                    if (token.isCancellationRequested) {
                         return null;
                    }
                    return client.protocol2CodeConverter.asWorkspaceEdit(result);
               },
               (error) => client.handleFailedRequest(RenameRequest.type, token, error.message, null, false)
          );
};