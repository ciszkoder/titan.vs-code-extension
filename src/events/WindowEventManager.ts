/******************************************************************************
 * Copyright (c) 2000-2024 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
/**
* @author Csilla Farkas
* @author Miklos Magyari
*/

import { window, TextEditorSelectionChangeEvent, TextEditor, ColorTheme, commands } from "vscode";
import { Event } from 'vscode-languageclient';
import DisposableManager from "../common/DisposableManager";
import { docCommentViewProvider } from "../document-comment-view/DocCommentViewProvider";
import { exclusionUtils } from "../file-exclusion/ExclusionUtils";

export class WindowEventManager extends DisposableManager {
    public register(event: Event<any>, handler: (event: any) => any ): void {
        const disposable = event(handler);
        this._disposables.push(disposable);
    }
}

const windowEventHandlers = {
    /** 
    * The cursor position is changed in the editor.
    * (Vscode treats cursor position as zero-length selection.)
    * If the position doesn't change in 1s we send request to the server, and display the document comment from the response on the wiew.
    **/
    sendRequestForDocumentComment: (event: TextEditorSelectionChangeEvent) => {
        docCommentViewProvider.sendRequestForDocCommentIfCursorPositionChanged(event);
    },

    /**
     * Visually indicate excluded files on the status bar when their editor is activated.
     * 
     * @param event 
     * @returns 
     */
    editorActivated: (event: TextEditor | null) => {
        exclusionUtils.showExcludedLabelOnStatusBar(event);
    },
    
    editorsChanged: (event: TextEditor[]) => {
        exclusionUtils.showExcludedLabelOnStatusBar(window.activeTextEditor);
    },

    themeChanged: (event: ColorTheme) => {
        commands.executeCommand('markdown.preview.refresh');
    }
};

export function registerWindowEvents(windowEventManager: WindowEventManager) {
    windowEventManager.register(window.onDidChangeTextEditorSelection, windowEventHandlers.sendRequestForDocumentComment);
    windowEventManager.register(window.onDidChangeActiveTextEditor, windowEventHandlers.editorActivated);
    windowEventManager.register(window.onDidChangeVisibleTextEditors, windowEventHandlers.editorsChanged);
    windowEventManager.register(window.onDidChangeActiveColorTheme, windowEventHandlers.themeChanged);
}
