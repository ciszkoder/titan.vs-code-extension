/******************************************************************************
 * Copyright (c) 2000-2024 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
/**
 * 
 * @author Miklos Magyari
 */

import { StatusBarAlignment, StatusBarItem, TextEditor, ThemeColor, window } from "vscode";
import { ExclusionState } from "./ExclusionState";

class ExclusionUtils {
     startusLabel: StatusBarItem;
     initialized = false;

     constructor() {
          this.startusLabel = window.createStatusBarItem(StatusBarAlignment.Right, 100);
          this.startusLabel.color = new ThemeColor('statusBarItem.warningForeground');
          this.startusLabel.backgroundColor = new ThemeColor('statusBarItem.warningBackground');
          this.startusLabel.text = 'Excluded file';
          this.startusLabel.tooltip = 'This file is excluded from build. Advanced code editing features will not work.';
     }

     public showExcludedLabelOnStatusBar(editor: TextEditor | null | undefined) {
          this.startusLabel.hide();
          if (editor === null || editor === undefined) {
               return;
          }

          if (ExclusionState.excludedItems.items.find(e => {
               return e.toString() === editor.document.uri.toString();
          }) !== undefined) {
               this.startusLabel.show();
          }
     }
}

export const exclusionUtils = new ExclusionUtils();