/******************************************************************************
 * Copyright (c) 2000-2024 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
/**
 * 
 * @author Csilla Farkas
 */

import { Uri } from 'vscode';
import { ExcludeFromBuildConfig } from '../common/Config';


export abstract class ExcludedItems {
    protected _items: Map<string, Uri> = new Map();
    
    get items(): Uri[] {
        return [...this._items.values()];
    }

    get fsPaths(): string[] {
        return [...this._items.keys()];
    }

    in(uri: Uri): boolean {
        return this._items.has(uri.fsPath);
    }

    add(param: Uri | Uri[]): void | Promise<void> {}

    remove(param?: Uri | Uri[]): void | Promise<void> {}
}

export class ExcludedItemsByTpd extends ExcludedItems {

    add(uris: Uri[]): void {
        this._items = new Map(uris.map(uri => [uri.fsPath, uri]));
    }

    remove(): void {
        this._items = new Map();
    }
}

export class ExcludedItemsInExplorer extends ExcludedItems {
    private _config = new ExcludeFromBuildConfig();
    constructor() {
        super();
        this._items = new Map<string, Uri>(this._config.content.map(uri => [uri.fsPath, uri]));
    }

    public async add(uris: Uri[]): Promise<void> {
        const size = this._items.size;
        uris.forEach(uri => {
            if(!this._items.has(uri.fsPath)) {
                this._items.set(uri.fsPath, uri);
            }
        });
    
        if(size < this._items.size) {
            await this._config.update(this.fsPaths);
        }
    }

    public async remove(uris: Uri[]): Promise<void> {
        const size = this._items.size;
        uris.forEach(uri => {
            this._items.delete(uri.fsPath);
        });

        if(size > this._items.size) {
            await this._config.update(this.fsPaths);
        }
    }
}