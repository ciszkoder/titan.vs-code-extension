/******************************************************************************
 * Copyright (c) 2000-2024 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
/**
 * 
 * @author Csilla Farkas
 */

import { Uri } from 'vscode';
import { ActivatedTpdConfig, ExcludeByTpdConfig } from '../common/Config';
import { TpdFile } from './exclude-by-tpd/TpdFile';
import { ExcludedItems, ExcludedItemsInExplorer, ExcludedItemsByTpd } from './ExcludedItems';

export class ExclusionState {
    private static _excludedItemsInExplorer: ExcludedItems = new ExcludedItemsInExplorer();
    private static _excludedItemsByTpd: ExcludedItems = new ExcludedItemsByTpd();
    private static _tempItems: Map<string, boolean> = new Map();

    private static _excludeByTpd = new ExcludeByTpdConfig();
    private static _activatedTpd = new ActivatedTpdConfig();

    static get tempExcludedItems(): Uri[] {
        return [...this._tempItems.entries()].filter(entry => entry[1]).map(entry => Uri.file(entry[0]));
    }

    static get tempIncludedItems(): Uri[] {
        return [...this._tempItems.entries()].filter(entry => !entry[1]).map(entry => Uri.file(entry[0]));
    }

    static getTempStatus(uri: Uri): boolean | void {
        if (this._tempItems.has(uri.fsPath)) {
            return this._tempItems.get(uri.fsPath);
        }
    }

    static get isEmptyTempExcludedItems(): boolean {
        return this._tempItems.size === 0;
    }

    static get excludedItems(): ExcludedItems {
        return this.isExclusionByTpd ? this._excludedItemsByTpd : this._excludedItemsInExplorer;
    }

    static get isExclusionByTpd(): boolean {
        return this._excludeByTpd.content;
    }

    static get activatedTpd(): TpdFile | void {
        return this._activatedTpd.content;
    }

    static async switchToExclusionByTpd(): Promise<void> {
        await this._excludeByTpd.update(true);
    }

    static async switchToExclusionInExplorer(): Promise<void> {
        await this._excludeByTpd.update(false);
    }
    
    static isExcluded(uri: Uri): boolean {
        return this.excludedItems.in(uri);
    }

    static isActivatedTpd(uri: Uri): boolean {
        const activatedTpd = this._activatedTpd.content;
        return activatedTpd ? activatedTpd.resourceUri.fsPath === uri.fsPath : false;
    }

    static async saveActivatedTpd(uri: Uri): Promise<void> {
        await this._activatedTpd.update(uri.fsPath);
    }

    static async resetActivatedTpd(): Promise<void> {
        await this._activatedTpd.update('');
    }

    static saveTempItems(uris: Uri[], isExcluded: boolean) {
        uris.forEach(item => {
            if (isExcluded === this.isExcluded(item)) {
                this._tempItems.delete(item.fsPath);
                return;
            }

            this._tempItems.set(item.fsPath, isExcluded);
        });
    }

    static clearTempItems() {
        this._tempItems.clear();
    }
}