/******************************************************************************
 * Copyright (c) 2000-2024 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
/**
 * @author Miklos Magyari
 * @author Csilla Farkas
 */

import { Uri, commands, Position, Range, Location, workspace, ExtensionContext, window, ViewColumn, WebviewPanel } from "vscode";
import { TpdFile } from "../file-exclusion/exclude-by-tpd/TpdFile";
import { mainExclusionManager } from "../file-exclusion/MainExclusionManager";
import { ClientManager } from "../client/ClientManager";
import { catchError } from "../common/Errors";
import { Command, CommandManager } from "./CommandManager";
import { DocCommentGenerator } from "../document-comment-generation/DocCommentGenerator";
import { Modal } from "../common/Modal";
import { ConfigKey } from "../common/types";
import markdownit = require("markdown-it/lib");
import { DocumentationViewer } from "../DocumentationViewer"

class ExcludeByTpdCommand implements Command {
	public readonly id = 'tpdview.excludeByTpd';

	@catchError
	public async execute() {
		if(!ClientManager.isReady()) {
			await Modal.featureIsNotAvailable("Exclusion by TPD");
			return;
		}
		await mainExclusionManager.useTpdForFileExclusion();
	}
}

class ExcludeInFileExplorerCommand implements Command {
	public readonly id = "tpdview.excludeInExplorer";

	@catchError
	public async execute() {
		if(!ClientManager.isReady()) {
			await Modal.featureIsNotAvailable("Exclusion in file explorer");
			return;
		}
		await mainExclusionManager.useExplorerForFileExclusion();
	}
}

class AddTpdFileCommand implements Command {
    public readonly id = 'tpdview.addFile';

	@catchError
    public async execute() {
		await mainExclusionManager.exclusionByTpd.addNewTpd();
    }
}

class OpenTpdFileCommand implements Command {
    public readonly id = 'tpdview.openFile';

	@catchError
    public async execute(tpd: TpdFile) {
		await mainExclusionManager.exclusionByTpd.openTpd(tpd.resourceUri);
    }
}

class AddAndOpenTpdFileCommand implements Command {
    public readonly id = 'tpdview.addAndOpenFile';

	@catchError
    public async execute() {
		await mainExclusionManager.exclusionByTpd.addNewTpd(true);
    }
}

class RemoveTpdFileCommand implements Command {
	public readonly id = 'tpdview.removeFile';

	@catchError
	public async execute(tpd: TpdFile) {
		await mainExclusionManager.exclusionByTpd.removeTpd(tpd);
	}
}

class ActivateTpdCommand implements Command {
	public readonly id = 'tpdview.activateTpd';

	@catchError
	public async execute(tpd: TpdFile) {
		if(!ClientManager.isReady()) {
			await Modal.featureIsNotAvailable("Exclusion by TPD");
			return;
		}
		await mainExclusionManager.exclusionByTpd.activateTpd(tpd);
	}
}

class DeactivateTpdCommand implements Command {
	public readonly id = 'tpdview.deactivateTpd';

	@catchError
	public async execute(tpd: TpdFile) {
		if(!ClientManager.isReady()) {
			await Modal.featureIsNotAvailable("Exclusion by TPD");
			return;
		}
		const configProp = workspace.getConfiguration(ConfigKey.compiler);
		if (configProp.get('analyzeOnlyWhenTpdIsActive') as boolean) {
			const result = await Modal.fullProjectAnalyzisIsForbidden("Deactivating a TPD file");
			if (result === 'Turn off and analyze project') {
				await configProp.update('analyzeOnlyWhenTpdIsActive', false);
			} else {
				return;
			}
		}

		await mainExclusionManager.exclusionByTpd.deactivateTpd();
	}
}

// class ExcludeFilesCommand implements Command {
// 	public readonly id = 'explorer.exclude';
	
// 	@catchError
// 	public async execute(clickedOn: Uri, selectedFiles: Uri[]) {
// 		if(!ClientManager.isReady()) {
// 			await Modal.featureIsNotAvailable("Exclusion in file explorer");
// 			return;
// 		}
// 		await mainExclusionManager.exclusionInExplorer.exclude(selectedFiles);
// 	}
// }

class TempExcludeFilesCommand implements Command {
	public readonly id = 'explorer.exclude';
	
	@catchError
	public async execute(clickedOn: Uri, selectedFiles: Uri[]) {
		await mainExclusionManager.exclusionInExplorer.tempExclude(selectedFiles);
	}
}

// class IncludeFilesCommand implements Command {
// 	public readonly id = 'explorer.include';

// 	@catchError
// 	public async execute(clickedOn: Uri, selectedFiles: Uri[]) {
// 		if(!ClientManager.isReady()) {
// 			await Modal.featureIsNotAvailable("Exclusion in file explorer");
// 			return;
// 		}
// 		await mainExclusionManager.exclusionInExplorer.include(selectedFiles);
// 	}
// }

class TempIncludeFilesCommand implements Command {
	public readonly id = 'explorer.include';

	@catchError
	public async execute(clickedOn: Uri, selectedFiles: Uri[]) {
		await mainExclusionManager.exclusionInExplorer.tempInclude(selectedFiles);
	}
}

class BuildCommand implements Command {
	public readonly id = 'titan.build';

	@catchError
	public async execute(clickedOn: Uri, selectedFiles: Uri[]) {
		if(!ClientManager.isReady()) {
			await Modal.featureIsNotAvailable("Exclusion in file explorer");
			return;
		}
		await mainExclusionManager.exclusionInExplorer.build();
	}
}

class GenerateDocumentCommentCommand implements Command {
	public readonly id = 'titan.generateDocumentComment';

	@catchError
	public async execute(file: Uri) {
		if(!ClientManager.isReady()) {
			await Modal.featureIsNotAvailable("Document comment generation");
			return;
		}
		const docCommentGenerator = new DocCommentGenerator();
		await docCommentGenerator.insertDocComment();
	}
}

class GetReferencesForCodelensCommand implements Command {
	public readonly id = 'titan.getReferencesForCodelens';

	@catchError
	public async execute(
		file: string,
		startPos: {readonly line: number, readonly character: number},
		locations: {
			uri: string,
			range: {
				start: {readonly line: number, readonly character: number},
				end: {readonly line: number, readonly character: number}
			}}[]
		) {
		if(!ClientManager.isReady()) {
			await Modal.featureIsNotAvailable("Codelens");
			return;
		}
		const uri: Uri = Uri.parse(file);
		const start: Position = new Position(startPos.line, startPos.character);
		const refLocations: Location[] = locations.map(loc => {
			const { uri, range } = loc;
			const locUri: Uri = Uri.parse(uri);
			const locRange: Range = new Range(range.start as Position, range.end as Position);
			const vscLoc: Location = new Location(locUri, locRange);
			return vscLoc;
		});

		await commands.executeCommand('editor.action.peekLocations', uri, start, refLocations);
	}
}

class OpenWalkthroughCommand implements Command {
	public readonly id = 'open-titan-walkthrough';

	@catchError
	public async execute() {
		commands.executeCommand(`workbench.action.openWalkthrough`, `titan-walkthrough`, false);
	}
}

class ShowDocumentation extends DocumentationViewer implements Command {
	public readonly id = 'titan.showDocumentation';

	@catchError
	public async execute() {
		this.showDocumentation();		
	}
}

export function registerCommands(commandManager: CommandManager, context: ExtensionContext) {
     commandManager.register(new AddTpdFileCommand());
	// commandManager.register(new ExcludeFilesCommand());
	// commandManager.register(new IncludeFilesCommand());
	commandManager.register(new TempExcludeFilesCommand());
	commandManager.register(new TempIncludeFilesCommand());
	commandManager.register(new BuildCommand());
	commandManager.register(new RemoveTpdFileCommand());
	commandManager.register(new ActivateTpdCommand());
	commandManager.register(new DeactivateTpdCommand());
	commandManager.register(new ExcludeByTpdCommand());
	commandManager.register(new ExcludeInFileExplorerCommand());
	commandManager.register(new OpenTpdFileCommand());
	commandManager.register(new AddAndOpenTpdFileCommand());
	commandManager.register(new GenerateDocumentCommentCommand());
	commandManager.register(new GetReferencesForCodelensCommand());
	commandManager.register(new OpenWalkthroughCommand());
	commandManager.register(new ShowDocumentation(context));
}
