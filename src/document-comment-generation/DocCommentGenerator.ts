/******************************************************************************
 * Copyright (c) 2000-2024 Ericsson Telecom AB
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/org/documents/epl-2.0/EPL-2.0.html
 ******************************************************************************/
/**
 * @author Csilla Farkas
 */

import { Position, TextDocument, TextLine, window } from "vscode";
import { clientManager } from "../client/ClientManager";


export class DocCommentGenerator {
    private editor;

    constructor() {
        this.editor = window.activeTextEditor;
    }

    private get indentation(): string {
        let indentation = '';
        if (this.editor) {
            const { document, selection } = this.editor;
            const textLine: TextLine = document.lineAt(selection.active.line);
            const { text, firstNonWhitespaceCharacterIndex } = textLine;
            if (textLine.isEmptyOrWhitespace) {
                return indentation;
            }
            const substringForInd = text.substring(0, firstNonWhitespaceCharacterIndex);
            if (substringForInd) {
                indentation = substringForInd;
            }
        }

        return indentation;
    }

    private get requestParams() {
        const { document, selection } = this.editor!;
        const requestParams = {
            identifier: { uri: document.uri.toString() },
            indentation: this.indentation,
            position: selection.active
        };

        return requestParams;
    }

    private async getDocComment(): Promise<string | void> {
        if(this.editor) {
            const docComment = await clientManager.generateDocComment(this.requestParams);
            return docComment;
        }

    }

    public async insertDocComment(): Promise<void> {
        const docComment = await this.getDocComment();
        if(docComment) {
            const activeLine = this.editor!.selection.active.line;
            const lineAbove = activeLine - 1;
            const newLine = '\n';
            let textToBeInserted = `${newLine}${this.indentation}${docComment.trim()}`;
            
            this.editor!.edit(editBuilder => {
                const lastCharIdxOfLineAbove = this.editor!.document.lineAt(lineAbove).range.end.character;
                if(lastCharIdxOfLineAbove) {
                    textToBeInserted = newLine + textToBeInserted;
                }
                editBuilder.insert(new Position(lineAbove, lastCharIdxOfLineAbove), textToBeInserted);
            });
        }
    }
}