# Titanium

Titanium is a quality analyzer tool for TTCN-3.

Capabilities:
- Detecting and reporting code smells in TTCN-3 and ASN.1 source codes
- Measuring and displaying code quality metrics on TTCN-3 source codes


Titanium Metrics is available through the activity bar:

![Titanium webview](../images/FullProjectMetricsView.png)