# File Exclusion in explorer
**Right-click** on file or folder in the EXPLORER view and select **"Exclude"** from the context menu.

![Exclusion in explorer](../images/SettingExclusionInExplorer.png)\
![Exclusion in explorer](../images/ExclusionInExplorer.png)




# File Exclusion by TPD file
Open the **TPD FILES** view in the EXPLORER, select a tpd file from your computer or from the previous ones, and **activate** it.

![Exclusion in tpd](../images/SettingExclusionByTpd.png)\
![Exclusion in tpd](../images/ActivateTpd.png)