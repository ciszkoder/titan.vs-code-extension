# Basic features

## Hover information

When hovering over an identifier anywhere in the code, information about the definition will be displayed. This information is based on the on-the-fly analysis of the code and document comment (if they exist for the given definition).

_Note_: hover information is not available for files excluded from the build.

![Hover info](img/hover-info.gif)

# Code navigation features

## Outline view

You can use the outline view in the Explorer to navigate to a definition.

Open **Outline**, find the definition and double-click on it. The cursor will jump to the definition in the editor.

_Note_: the outline will not display items from files excluded from the build.

![Outline: go to definition](img/outline-go-to-definition.gif)

## Go to definition

If you want to navigate to the location where a certain identifier is defined, use the **go to definition** feature. 

Move the cursor to the identifier and either use the hotkey **F12** or right click to open the context menu and click **Go to Definition**. The cursor will jump to the original definition of the identifier. This works across modules as well: if the definition resides in another file, that file will be opened in the editor.

_Note_: 'Go to definition' will not work if the currently edited file is excluded from the build.

![Go to definition](img/go-to-definition.gif)

## Peek references

You can get an inline list of references for a given symbol using the **Peek references** feature.

Place the cursor on an identifier in the editor, right click to open the context menu and choose **Peek > Peek References**. A list of all references for the identifier will be listed. Using a single click you can jump to the reference in the inline view and by double clicking you can open the reference in the editor.

![Peek references](img/peek-references.gif)

# TTCN specific features
## Generate document comment

Titan supports TTCN3 document comments specified by ETSI standard [ES 201 873-10](https://www.etsi.org/deliver/etsi_es/201800_201899/20187310/04.05.01_60/es_20187310v040501p.pdf). 

To generate a document comment template for a definition, hover over it, right click to open the context menu and choose **Generate document comment**. This will insert a comment template above the definition with definition specific  tags containing information based on code analysis.

For example, _function_ or _testcase_ comments will include a **@param** tag for each formal parameter.

![Generate document comment](img/generate-doc-comment.gif)
