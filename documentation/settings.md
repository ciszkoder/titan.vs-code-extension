# Titan extension settings

Titan extension has several settings controlling its behaviour.
To edit these settings, open the settings editor either
by using a hotkey (pressing **Ctrl + ,**) or from the menu **(File / Preferences / Settings**).

In the search box, type '**titan.** ' to find titan specifice settings.

## Titan settings

- **titan.compiler.titanInstallationPath** (string) Path where titan.core compiler is installed. _Currently unused_.
- **titan.compiler.activatedTpd** (string) Full path of activated TPD file. Typically this should not be edited manually because the extension has an explorer view to manage TPD files.
- **titan.compiler.analyzeOnlyWhenTpdIsActive** (boolean) If set to _true_, the project will only be built if there is an active TPD file.
- **titan.compiler.excludeByTpd** (boolean) If set to _true_, file and folder excludes will be controlled by the active TPD file and exclusion from the file explorer will be disabled.
- **titan.compiler.licenseFilePath** Path for the titan license file. _Currently unused_.
- **titan.compiler.tpdFiles** List of TPD files added to the workspace. Should not be edited manually; the explorer view for TPD files should be used instead.
- **titan.enableInlayHints** (boolean) Controls whether inlay hints should be displayed in the editor for **TTCN3** files.
- **titan.enableTitanium** (boolean) If set to _true_, **titanium** static code analyzer will check TTCN3 files for _code smells_ and displays warnings for problems found.
- **titan.extension.connection.connectionKind** Specifies the communication method between the extension and the language server (__tcp__ or __stdio__).
- **titan.extension.connection.logDir** Path of directory where language server debug logs are stored.
- **titan.extension.connection.serverPort** TCP port used for communication between the extension and the language server. Change it if the port is used by another application or alternatively, switch to sdio communication mode.
- **titan.extension.excludedFromBuild** List of files and directories that are manually excluded from the build. Should not be edited manually; to edit excludes, use context menus in the file explorer.
- **titan.onTheFlyChecker.enableNamingConventionCheck** (boolean) If set to _true_, TTCN3 identifiers are checked against naming conventions (for example, _testcase_ names should begin with '**tc_**'. 
- **titan.onTheFlyChecker.namingConventionSeverity** ('warning' or 'error') Severity of problems caused by naming convention violations.
- **Outline settings**: boolean values to control if certain TTCN3 definitions should be shown in the outline view.
    - **titan.outline.showClasses** Show _classes_ in the outline
    - **titan.outline.showFunctions** Show _functions_ in the outline
    - **titan.outline.showTestcases** Show _testcases_ in the outline
